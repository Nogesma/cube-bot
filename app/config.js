import { range } from "ramda";

const events = [
  "333",
  "222",
  "444",
  "555",
  "3BLD",
  "OH",
  "SQ1",
  "MEGA",
  "PYRA",
  "CLOCK",
  "SKEWB",
];
const hours = range(1)(24);

export { events, hours };
